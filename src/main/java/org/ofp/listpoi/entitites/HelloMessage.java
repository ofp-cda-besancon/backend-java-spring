package org.ofp.listpoi.entitites;


public class HelloMessage {

    private int id;
    private String name;

    public HelloMessage(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
