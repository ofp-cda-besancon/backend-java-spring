package org.ofp.listpoi.entitites;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;

@Entity
@SuppressWarnings("unused")
public class POI {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private float rate;

    @Column
    private String latitude;

    @Column
    private String longitude;

    @Column(length = 100)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Lob
    @Column(columnDefinition = "BLOB")
    private byte[] photo;

    @OneToOne(optional = true)
    private TemporaryEvent temporaryEvent;

    @ManyToMany
    private Collection<Category> categories;


    /**
     * Private default constructor for JPA
     */
    @SuppressWarnings("unused")
    private POI() { }

    /**
     * Simple constructor without id
     */
    public POI(float rate, String latitude, String longitude, String title, String description, byte[] photo) {
        this.rate = rate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.description = description;
        this.photo = photo;
    }

    /**
     * Constructor with id for use in unit tests
     */
    public POI(int id, float rate, String latitude, String longitude, String title, String description, byte[] photo) {
        this.id = id;
        this.rate = rate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.description = description;
        this.photo = photo;

        setCategories(Arrays.asList(new Category("", "")));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public TemporaryEvent getTemporaryEvent() {
        return temporaryEvent;
    }

    public void setTemporaryEvent(TemporaryEvent temporaryEvent) {
        this.temporaryEvent = temporaryEvent;
    }

    public Collection<Category> getCategories() {
        return categories;
    }

    public void setCategories(Collection<Category> categories) {
        this.categories = categories;
    }
}
