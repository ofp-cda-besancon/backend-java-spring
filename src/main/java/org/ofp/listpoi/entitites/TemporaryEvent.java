package org.ofp.listpoi.entitites;

import javax.persistence.*;
import java.util.Date;

@Entity
@SuppressWarnings("unused")
public class TemporaryEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Date startDate;

    @Column
    private Date endDate;


    /**
     * Private default constructor for JPA
     */
    @SuppressWarnings("unused")
    private TemporaryEvent() { }

    public TemporaryEvent(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
