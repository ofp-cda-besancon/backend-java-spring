package org.ofp.listpoi.entitites;

import javax.persistence.*;

@Entity
@SuppressWarnings("unused")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "TEXT")
    private String text;

    @Column
    private int rate;

    @ManyToOne()
    @JoinColumn(name = "poi_id")
    private POI poi;


    /**
     * Private default constructor for JPA
     */
    @SuppressWarnings("unused")
    private Rating() { }

    public Rating(POI poi, String text, int rate) {
        this.poi = poi;
        this.text = text;
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public POI getPoi() {
        return poi;
    }

    public void setPoi(POI poi) {
        this.poi = poi;
    }
}
