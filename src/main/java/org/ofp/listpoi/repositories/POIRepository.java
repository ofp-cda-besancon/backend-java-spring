package org.ofp.listpoi.repositories;

import org.ofp.listpoi.entitites.POI;
import org.springframework.data.repository.CrudRepository;

public interface POIRepository extends CrudRepository<POI, Integer> {
}
