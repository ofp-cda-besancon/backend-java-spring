package org.ofp.listpoi.repositories;

import org.ofp.listpoi.entitites.Rating;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface RatingRepository extends CrudRepository<Rating, Integer> {
    Collection<Rating> findByPoiId(int id);
}
