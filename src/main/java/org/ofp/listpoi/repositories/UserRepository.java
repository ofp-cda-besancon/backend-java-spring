package org.ofp.listpoi.repositories;

import org.ofp.listpoi.entitites.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByUsername(String username);

    Optional<User> findByEmail(String email);

    @Query("select u from User u where u.firstName like %?1% or u.lastName like %?1% or u.username like %?1%")
    Iterable<User> findByName(String name);

}
