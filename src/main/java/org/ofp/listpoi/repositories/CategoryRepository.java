package org.ofp.listpoi.repositories;

import org.ofp.listpoi.entitites.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
