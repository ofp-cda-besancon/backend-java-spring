package org.ofp.listpoi.repositories;

import org.ofp.listpoi.entitites.TemporaryEvent;
import org.springframework.data.repository.CrudRepository;

public interface TemporaryEventRepository extends CrudRepository<TemporaryEvent, Integer> {
}
