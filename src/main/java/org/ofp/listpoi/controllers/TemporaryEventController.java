package org.ofp.listpoi.controllers;

import org.ofp.listpoi.entitites.TemporaryEvent;
import org.ofp.listpoi.repositories.TemporaryEventRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class TemporaryEventController {

    private TemporaryEventRepository temporaryEventRepository;

    public TemporaryEventController(TemporaryEventRepository temporaryEventRepository) {
        this.temporaryEventRepository = temporaryEventRepository;
    }

    // curl -X GET "http://localhost:8080/api/v1/temporaryEvents"
    @GetMapping("/api/v1/temporaryEvents")
    public Iterable<TemporaryEvent> listTemporaryEvents() {
        return temporaryEventRepository.findAll();
    }

    // curl -X GET "http://localhost:8080/api/v1/temporaryEvents/1"
    @GetMapping("/api/v1/temporaryEvents/{id}")
    public TemporaryEvent getTemporaryEvent(@PathVariable int id) {
        return temporaryEventRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    // curl -X POST -d '{"name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/temporaryEvents"
    @PostMapping(path = "/api/v1/temporaryEvents",  headers = "Accept=application/json")
    public TemporaryEvent createTemporaryEvent(@RequestBody TemporaryEvent temporaryEvent) {
        return temporaryEventRepository.save(temporaryEvent);
    }

    // curl -X PUT -d '{"id": "1", "name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/temporaryEvents/1"
    @PutMapping(path = "/api/v1/temporaryEvents/{id}",  headers = "Accept=application/json")
    public TemporaryEvent updateTemporaryEvent(@RequestBody TemporaryEvent temporaryEvent, @PathVariable int id) {
        if(!temporaryEventRepository.existsById(id))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        temporaryEvent.setId(id); // !!! Override id using url parameters to prevent potential injection using json values
        return temporaryEventRepository.save(temporaryEvent);
    }

    // curl -X DELETE "http://localhost:8080/api/v1/temporaryEvents/1"
    @DeleteMapping("/api/v1/temporaryEvents/{id}")
    public void deleteTemporaryEvent(@PathVariable int id) {
        temporaryEventRepository.deleteById(id);
    }


}
