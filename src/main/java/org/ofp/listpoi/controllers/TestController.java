package org.ofp.listpoi.controllers;

import org.ofp.listpoi.entitites.HelloMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController {

    // curl http://localhost:8080/hello
    @GetMapping("/hello")
    public HelloMessage hello(@RequestParam(name = "user", defaultValue = "world") String user) {
        return new HelloMessage(1, String.format("Hello %s", user));
    }

}
