package org.ofp.listpoi.controllers;

import org.ofp.listpoi.entitites.POI;
import org.ofp.listpoi.repositories.POIRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class POIController {

    private POIRepository poiRepository;

    public POIController(POIRepository categoryRepository) {
        this.poiRepository = categoryRepository;
    }

    // curl -X GET "http://localhost:8080/api/v1/pois"
    @GetMapping("/api/v1/pois")
    public Iterable<POI> listPOIs() {
        return poiRepository.findAll();
    }

    // curl -X GET "http://localhost:8080/api/v1/pois/1"
    @GetMapping("/api/v1/pois/{id}")
    public POI getPOIs(@PathVariable int id) {
        return poiRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    // curl -X POST -d '{"rate": 3.5, "latitude": "lat", "longitude": "lon", "title": "mon titre", "description": "desc"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/pois"
    @PostMapping(path = "/api/v1/pois",  headers = "Accept=application/json")
    public POI createPOI(@RequestBody POI category) {
        return poiRepository.save(category);
    }

    // curl -X PUT -d '{"id": "1", "name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/pois/1"
    @PutMapping(path = "/api/v1/pois/{id}",  headers = "Accept=application/json")
    public POI updatePOI(@RequestBody POI category, @PathVariable int id) {
        if(!poiRepository.existsById(id))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        category.setId(id); // !!! Override id using url parameters to prevent potential injection using json values
        return poiRepository.save(category);
    }

    // curl -X DELETE "http://localhost:8080/api/v1/pois/1"
    @DeleteMapping("/api/v1/pois/{id}")
    public void deletePOI(@PathVariable int id) {
        poiRepository.deleteById(id);
    }

}