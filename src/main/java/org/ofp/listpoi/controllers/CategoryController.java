package org.ofp.listpoi.controllers;

import org.ofp.listpoi.entitites.Category;
import org.ofp.listpoi.repositories.CategoryRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class CategoryController {

    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    // curl -X GET "http://localhost:8080/api/v1/categories"
    @GetMapping("/api/v1/categories")
    public Iterable<Category> listCategories() {
        return categoryRepository.findAll();
    }

    // curl -X GET "http://localhost:8080/api/v1/categories/1"
    @GetMapping("/api/v1/categories/{id}")
    public Category getCategory(@PathVariable int id) {
        return categoryRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    // curl -X POST -d '{"name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/categories"
    @PostMapping(path = "/api/v1/categories",  headers = "Accept=application/json")
    public Category createCategory(@RequestBody Category category) {
        return categoryRepository.save(category);
    }

    // curl -X PUT -d '{"id": "1", "name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/categories/1"
    @PutMapping(path = "/api/v1/categories/{id}",  headers = "Accept=application/json")
    public Category updateCategory(@RequestBody Category category, @PathVariable int id) {
        if(!categoryRepository.existsById(id))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        category.setId(id); // !!! Override id using url parameters to prevent potential injection using json values
        return categoryRepository.save(category);
    }

    // curl -X DELETE "http://localhost:8080/api/v1/categories/1"
    @DeleteMapping("/api/v1/categories/{id}")
    public void deleteCategory(@PathVariable int id) {
        categoryRepository.deleteById(id);
    }


}
