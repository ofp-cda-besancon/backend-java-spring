package org.ofp.listpoi.controllers;

import org.ofp.listpoi.entitites.Rating;
import org.ofp.listpoi.repositories.POIRepository;
import org.ofp.listpoi.repositories.RatingRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Iterator;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class RatingController {

    private final RatingRepository ratingRepository;
    private final POIRepository poiRepository;


    public RatingController(RatingRepository ratingRepository, POIRepository poiRepository) {
        this.ratingRepository = ratingRepository;
        this.poiRepository = poiRepository;
    }

    // curl -X GET "http://localhost:8080/api/v1/ratings"
    @GetMapping("/api/v1/ratings")
    public Iterable<Rating> listRatings() {
        return ratingRepository.findAll();
    }

    // curl -X GET "http://localhost:8080/api/v1/ratings/1"
    @GetMapping("/api/v1/ratings/{id}")
    public Rating getRating(@PathVariable int id) {
        return ratingRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    // curl -X POST -d '{"name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/ratings"
    @PostMapping(path = "/api/v1/ratings",  headers = "Accept=application/json")
    public Rating createRating(@RequestBody Rating rating) {
        Rating r = ratingRepository.save(rating);

        // Compute new rating
        float sum = 0;
        Collection<Rating> ratings = ratingRepository.findByPoiId(r.getPoi().getId());
        for (Rating value : ratings) {
            sum += value.getRate();
        }
        float newRating = sum / ratings.size();

        // Cache and persist new rating
        r.getPoi().setRate(newRating);
        poiRepository.save(r.getPoi());

        return r;
    }

    // curl -X PUT -d '{"id": "1", "name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/ratings/1"
    @PutMapping(path = "/api/v1/ratings/{id}",  headers = "Accept=application/json")
    public Rating updateRating(@RequestBody Rating rating, @PathVariable int id) {
        if(!ratingRepository.existsById(id))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        rating.setId(id); // !!! Override id using url parameters to prevent potential injection using json values
        return ratingRepository.save(rating);
    }

    // curl -X DELETE "http://localhost:8080/api/v1/ratings/1"
    @DeleteMapping("/api/v1/ratings/{id}")
    public void deleteRating(@PathVariable int id) {
        ratingRepository.deleteById(id);
    }


}
