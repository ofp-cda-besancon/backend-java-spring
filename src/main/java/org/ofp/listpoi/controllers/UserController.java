package org.ofp.listpoi.controllers;

import org.ofp.listpoi.entitites.User;
import org.ofp.listpoi.repositories.UserRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class UserController {

    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // curl -X GET "http://localhost:8080/api/v1/users"
    @GetMapping("/api/v1/users")
    public Iterable<User> listUsers(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "email", required = false) String email) {
        if(name != null) {
            return userRepository.findByName(name);
        } else if(email != null) {
            return List.of(userRepository.findByEmail(email).get());
        } else {
            return userRepository.findAll();
        }
    }

    // curl -X GET "http://localhost:8080/api/v1/users/1"
    @GetMapping("/api/v1/users/{id}")
    public User getUser(@PathVariable int id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    // curl -X POST -d '{"name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/users"
    @PostMapping(path = "/api/v1/users",  headers = "Accept=application/json")
    public User createUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    // curl -X PUT -d '{"id": "1", "name": "test", "icon": "home"}' -H "Content-type: application/json" "http://localhost:8080/api/v1/users/1"
    @PutMapping(path = "/api/v1/users/{id}",  headers = "Accept=application/json")
    public User updateUser(@RequestBody User user, @PathVariable int id) {
        if(!userRepository.existsById(id))
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");

        user.setId(id); // !!! Override id using url parameters to prevent potential injection using json values
        return userRepository.save(user);
    }

    // curl -X DELETE "http://localhost:8080/api/v1/users/1"
    @DeleteMapping("/api/v1/users/{id}")
    public void deleteUser(@PathVariable int id) {
        userRepository.deleteById(id);
    }


}
