package org.ofp.listpoi.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ofp.listpoi.entitites.User;
import org.ofp.listpoi.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@DataJpaTest
public class UserTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    /*
     * Static field used to store the id of the user created by the `setup` call
     */
    private static int currentId = 0;


    @BeforeEach
    public void setup() {
        User u = new User(
                "mlhomme",
                "password",
                "Michaël",
                "Lhomme",
                "m.lhomme@onlineformapro.com");

        currentId = entityManager.persistAndGetId(u, Integer.class);
        entityManager.flush();
    }

    @Test
    public void canCreateUser() {
        User u = new User(
                "mlhomme2",
                "password",
                "Michaël",
                "Lhomme",
                "m.lhomme@onlineformapro.com");

        entityManager.persist(u);
        entityManager.flush();

        User found = userRepository.findByUsername("mlhomme2");
        assertEquals(u.getUsername(), found.getUsername());
    }

    @Test
    public void cannotCreateUserWithSameUsername() {
        User u = new User(
                "mlhomme",
                "password",
                "Michaël",
                "Lhomme",
                "m.lhomme@onlineformapro.com");

        assertThrows(Exception.class, () -> {
            entityManager.persist(u);
            entityManager.flush();
        });
    }

    @Test
    public void canRetrieveUserById() {
        User found = userRepository.findById(currentId).orElseThrow();
        assertEquals("mlhomme", found.getUsername());
    }

    @Test
    public void canRetrieveUserByUsername() {
        User found = userRepository.findByUsername("mlhomme");
        assertEquals("mlhomme", found.getUsername());
    }

    @Test
    public void canDeleteUserById() {
        userRepository.deleteById(currentId);
    }

    @Test
    public void canUpdateUser() {
        User found = userRepository.findById(currentId).orElseThrow();
        found.setUsername("newusername");
        userRepository.save(found);

        found = userRepository.findById(currentId).orElseThrow();
        assertEquals("newusername", found.getUsername());
    }

}
