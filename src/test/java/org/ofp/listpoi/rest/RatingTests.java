package org.ofp.listpoi.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.ofp.listpoi.controllers.RatingController;
import org.ofp.listpoi.entitites.POI;
import org.ofp.listpoi.entitites.Rating;
import org.ofp.listpoi.repositories.POIRepository;
import org.ofp.listpoi.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@Import(RestTestsConfiguration.class)
@WebMvcTest(RatingController.class)
class RatingTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RatingRepository ratingRepository;

    @MockBean
    private POIRepository poiRepository;

    @Test
    public void newRatingUpdateThePOICachedRate() throws Exception {
        POI refPOI = new POI(1, 0, "lat", "lon", "title", "desc", null);
        Rating newRating = new Rating(refPOI, "comment", 5);

        List<Rating> existingRatings = new LinkedList<>();
        existingRatings.add(new Rating(refPOI, "comment2", 3));
        existingRatings.add(new Rating(refPOI, "comment3", 4));
        existingRatings.add(newRating); // also add the new rating to the list as it is persisted before retrieving the list of POI (in RatingController)

        // Setup mock repository to return the parameters it received when `save(parameter)` is called
        when(ratingRepository.save(Mockito.any(Rating.class))).then(i -> i.getArguments()[0]);
        // Setup mock repository to return the above list of ratings when `findByPoiId(1)` is called
        given(ratingRepository.findByPoiId(1)).willReturn(existingRatings);

        mvc.perform(post("/api/v1/ratings")
                .content(new ObjectMapper().writeValueAsString(newRating))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Setup mock poiRepository to intercept the POI update and assert the new computed rating
        ArgumentCaptor<POI> argumentCaptor = ArgumentCaptor.forClass(POI.class);
        verify(poiRepository).save(argumentCaptor.capture());
        assertEquals(4, argumentCaptor.getValue().getRate());
    }

}
