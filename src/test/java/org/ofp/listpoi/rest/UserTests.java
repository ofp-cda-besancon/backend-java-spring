package org.ofp.listpoi.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.ofp.listpoi.controllers.UserController;
import org.ofp.listpoi.entitites.User;
import org.ofp.listpoi.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser
@Import(RestTestsConfiguration.class)
@WebMvcTest(UserController.class)
public class UserTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    private static final User defaultUser = new User(
            "mlhomme",
            "password",
            "Michaël",
            "Lhomme",
            "m.lhomme@onlineformapro.com"
    );


    @Test
    @WithAnonymousUser
    public void cannotCreateWhenNotAuthenticated() throws Exception {
        mvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(defaultUser)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canCreateUser() throws Exception {
        // Setup mock repository to return `u` when `save()` is called
        given(userRepository.save(Mockito.any(User.class))).willReturn(defaultUser);

        mvc.perform(post("/api/v1/users")
                .content(new ObjectMapper().writeValueAsString(defaultUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.username", is("mlhomme")));
    }



    @Test
    @WithAnonymousUser
    public void cannotGetWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/users/1")).andExpect(status().isForbidden());
    }


    @Test
    public void cannotGetNonExistingUser() throws Exception {
        mvc.perform(get("/api/v1/users/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    public void canGetUserById() throws Exception {
        // Setup mock repository to return the `u` when `findById()` is called
        given(userRepository.findById(Mockito.anyInt())).willReturn(java.util.Optional.of(defaultUser));

        mvc.perform(get("/api/v1/users/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.username", is("mlhomme")));
    }


    @Test
    @WithAnonymousUser
    public void cannotListWhenNotAuthenticated() throws Exception {
        mvc.perform(get("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }


    @Test
    public void canListUser() throws Exception {
        List<User> allUsers = new LinkedList<>();
        allUsers.add(new User( "mlhomme", "password", "Michaël", "Lhomme", "m.lhomme@onlineformapro.com"));
        allUsers.add(new User( "mlhomme2", "password2", "Michaël2", "Lhomme2", "m.lhomme2@onlineformapro.com"));

        // Setup mock repository to return the `allUsers` list when `findAll()` is called
        given(userRepository.findAll()).willReturn(allUsers);

        mvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username", is("mlhomme")))
                .andExpect(jsonPath("$[1].username", containsString("mlhomme2")));
    }


    @Test
    @WithAnonymousUser
    public void cannotDeleteWhenNotAuthenticated() throws Exception {
        mvc.perform(delete("/api/v1/users/1")).andExpect(status().isForbidden());
    }


    @Test
    public void canDeleteUser() throws Exception {
        mvc.perform(delete("/api/v1/users/1"))
                .andExpect(status().isOk());

        // Check in mock repository that the `deleteById` method has been called with `1` as parameter
        verify(userRepository).deleteById(1);
    }


    @Test
    @WithAnonymousUser
    public void cannotUpdateWhenNotAuthenticated() throws Exception {
        mvc.perform(put("/api/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(defaultUser)))
                .andExpect(status().isForbidden());
    }


    @Test
    public void cannotUpdateNonExistingUser() throws Exception {
        mvc.perform(put("/api/v1/users/1")
                .content(new ObjectMapper().writeValueAsString(defaultUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


    @Test
    public void canUpdateUser() throws Exception {
        // Setup mock repository to return the `true` when `existsById(1)` is called
        given(userRepository.existsById(1)).willReturn(true);
        // Setup mock repository to return the parameters it received when `save(parameter)` is called
        when(userRepository.save(Mockito.any(User.class))).then(i -> i.getArguments()[0]);

        mvc.perform(put("/api/v1/users/1")
                .content(new ObjectMapper().writeValueAsString(defaultUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.username", is("mlhomme")));
    }

}
