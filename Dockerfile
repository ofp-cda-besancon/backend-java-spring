# Build the package
FROM openjdk:16-jdk-alpine AS build
COPY . /home/listpoi
WORKDIR /home/listpoi
RUN ./gradlew --no-daemon build

# Switch to runtime
FROM openjdk:16-jdk-alpine
RUN mkdir /home/listpoi
COPY --from=build /home/listpoi/build/libs/listpoi-0.0.1-SNAPSHOT.jar /home/listpoi
WORKDIR /home/listpoi
CMD java -jar listpoi-0.0.1-SNAPSHOT.jar
